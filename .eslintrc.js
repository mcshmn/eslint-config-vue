module.exports = {
	extends: [
		'@my/eslint-config-main',
		'plugin:vue/essential',
	],
	rules: {
		'vue/script-indent': [
			'error',
			'tab',
		],
		'vue/html-indent': [
			'error',
			'tab',
		],
	},
	parserOptions: {parser: 'babel-eslint'},
}
